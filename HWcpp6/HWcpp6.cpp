﻿#include <fstream>
#include <iostream>
#include "pch.h"

static char s_cDelim[] = " ,.-+<>_=#$%^&*()@<>\'\"\n/[]{}|\r\t!\?:1234567890\xb7";
static const int sc_nSizeLine = 256;

using namespace std;

void vFillFile(ofstream* fOutput)
{
	int nRandLen = 50 + rand() % 50;
	char cRandInpt;

	for (int i = 0; i < nRandLen; i++)
	{
		cRandInpt = 32 + rand() % 95;
		*fOutput << cRandInpt;
	}
}

uint32_t nGetLength(ifstream* fInput)
{
	char cInpt;
	uint32_t nLength = 0;

	while (*fInput >> cInpt)
		nLength++;

	fInput->clear();

	fInput->seekg(0, fInput->beg);

	return nLength;
}

void vUniteFiles(ifstream* fFirst, ifstream* fSecond, ofstream* fOutput)
{
	uint32_t nFirstLen, nSecondLen;
	char* pcBuff = nullptr;

	nFirstLen = nGetLength(fFirst);
	nSecondLen = nGetLength(fSecond);

	pcBuff = (char*) calloc(nFirstLen + nSecondLen, sizeof(char));
	
	for (int i = 0; i < (int) nFirstLen; i++)
		fFirst->get(pcBuff[i]);

	for (int i = 0 ; i < (int) nSecondLen; i++)
		fSecond->get(pcBuff[nFirstLen + i]);

	for (int i = 0; i < (int) (nFirstLen + nSecondLen); i++)
		* fOutput << pcBuff[i];
}

bool blFindFile(ifstream* fInput, char* pcFindWord)
{
	char* pcBuff = (char*)calloc(sc_nSizeLine + 1, sizeof(char));
	char* pcWord;

	char* pcFoundWord = pcFindWord;
	int nFoundWordLen = strlen(pcFoundWord);

	for (int i = 0; i < nFoundWordLen; i++)
		pcFoundWord[i] = tolower(pcFoundWord[i]);

	while (fInput->getline(pcBuff, sc_nSizeLine))
	{
		pcWord = strtok(pcBuff, s_cDelim);

		while (pcWord != nullptr)
		{
			if (strlen(pcWord) != nFoundWordLen)
			{
				pcWord = strtok(nullptr, s_cDelim);
				continue;
			}

			for (int i = 0; i < nFoundWordLen; i++)
				pcWord[i] = tolower(pcWord[i]);

			if (!(strcmp(pcWord, pcFoundWord)))
				return true;

			pcWord = strtok(nullptr, s_cDelim);
		}
	}

	return false;
}

int main(int argc, char* argv[])
{
	//part 1

	ofstream fFirstOut("first.txt", ofstream::out | ofstream::trunc);
	ofstream fSecondOut("second.txt", ofstream::out | ofstream::trunc);

	if ((!fFirstOut.is_open()) || (!fSecondOut.is_open()))
	{
		printf("Error. Filestreams didn't open\n");
		return -1;
	}

	vFillFile(&fFirstOut);
	vFillFile(&fSecondOut);
	
	fFirstOut.close();
	fSecondOut.close();

	//part 2

	ifstream fFirstIn("first.txt");
	ifstream fSecondIn("second.txt");
	ofstream fThirdOut("FirstAndSecond.txt", ofstream::out | ofstream::trunc);

	if ((!fFirstIn.is_open()) || (!fSecondIn.is_open()) || (!fThirdOut.is_open()))
	{
		printf("Error. Filestreams didn't open\n");
		return -1;
	}

	vUniteFiles(&fFirstIn, &fSecondIn, &fThirdOut);

	fFirstIn.close();
	fSecondIn.close();
	fThirdOut.close();

	//part 3
	if (argc > 1)
	{
		if (argc < 3)
		{
			printf("Error.Wrong Number of arguments\n");
			return -1;
		}

		ifstream fSearchStream(argv[1]);
		
		if (!fSearchStream.is_open())
		{
			printf("Error. Wrong file PATH\n");
			return -1;
		}

		if (blFindFile(&fSearchStream, argv[2]))
			printf("Word found\n");
		else
			printf("Word not found\n");
	}

}
